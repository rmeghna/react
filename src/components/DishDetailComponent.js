import React, { Component }  from 'react';
import { Card, CardImg, CardText, CardBody, CardTitle, Breadcrumb, BreadcrumbItem,
          Button, Modal, ModalHeader, ModalBody, Row, Col, Label } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Control, LocalForm, Errors } from 'react-redux-form';
import { Loading } from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';
import { FadeTransform, Fade, Stagger } from 'react-animation-components';

const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => val && (val.length >= len);

class CommentForm extends Component {
  constructor(props) {
      super(props);
      this.toggleModal = this.toggleModal.bind(this);
      this.props = props; //, this.values.rating, this.values.author, this.values.comment);
      this.state = {
          author: '',
          touched: {
              author: false
          },
          isModalOpen: false
      };

      this.handleInputChange = this.handleInputChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
  }

  toggleModal() {
    this.setState({
      isModalOpen: !this.state.isModalOpen
    });
  
  }

  validate(author) {

      const errors = {
          author: ''
      };

      if (this.state.touched.author && author.length < 3)
          errors.auhtor = 'Auhtor should be >= 3 characters';
      else if (this.state.touched.author && author.length > 15)
          errors.author = 'Author should be <= 15 characters';

      return errors;
  }

  handleInputChange(event) {
      const target = event.target;
      const value = target.type === 'checkbox' ? target.checked : target.value;
      const name = target.name;
  
      this.setState({
        [name]: value
      });
  }

  handleSubmit(values) {
      this.toggleModal();
      console.log('Current State is: ' + JSON.stringify(values));
      //alert('Current State is: ' + JSON.stringify(values));
      console.log("this.props.dish ::", this.props.dish);
      this.props.postComment(this.props.dish.id, values.rating, values.author, values.comment);
      // event.preventDefault();
  }

  render() {
    if (this.props.isLoading) {
        return(
            <div className="container">
                <div className="row">            
                    <Loading />
                </div>
            </div>
        );
    }
    else if (this.props.errMess) {
        return(
            <div className="container">
                <div className="row">            
                    <h4>{this.props.errMess}</h4>
                </div>
            </div>
        );
    }
    else if (this.props.dish){
      return (
          <div className="container">
            <div className="row">
                <Breadcrumb>
                    <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                    <BreadcrumbItem active>{this.props.dish.name}</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12">
                    <h3>{this.props.dish.name}</h3>
                    <hr />
                </div>                
            </div>
            <div className="row">
                <div className="col-12 col-md-5 m-1">
                    <RenderDish dish={this.props.dish} />
                </div>
                <div className="col-12 col-md-5 m-1">
                    <RenderComments myComments={this.props.comments} 
                      tg={this.toggleModal} 
                    />
                </div>
            </div>
            <div>
                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                  <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
                  <ModalBody>
                          <LocalForm onSubmit={(values) => this.handleSubmit(values)}>
                            <Row className="form-group">
                              <Label htmlFor="rating" md={2}>Rating</Label>
                              <Col md={10}>
                                  <Control.select model=".rating" name="rating"
                                          className="form-control">
                                      <option>1</option>
                                      <option>2</option>
                                      <option>3</option>
                                      <option>4</option>
                                      <option>5</option>
                                  </Control.select>
                              </Col>
                            </Row>
                            <Row className="form-group">
                              <Label htmlFor="author" md={2}>Author</Label>
                              <Col md={10}>
                                  <Control.text model=".author" id="author" name="author"
                                      placeholder="Author"
                                      className="form-control"
                                      validators={{
                                          required, minLength: minLength(3), maxLength: maxLength(15)
                                      }}
                                       />
                                  <Errors
                                      className="text-danger"
                                      model=".author"
                                      show="touched"
                                      messages={{
                                          required: 'Required ',
                                          minLength: 'Must be greater than 2 characters',
                                          maxLength: 'Must be 15 characters or less'
                                      }}
                                   />
                              </Col>
                          </Row>
                          <Row className="form-group">
                              <Label htmlFor="comment" md={2}>Comment</Label>
                              <Col md={10}>
                                  <Control.textarea model=".comment" id="comment" name="comment"
                                      rows="12"
                                      className="form-control" />
                              </Col>
                          </Row>
                          <Button type="submit" color="primary">
                            Submit
                          </Button>
                        </LocalForm>
                </ModalBody>
              </Modal>
            </div>
          </div>
      );
    }
    else {
      return (<div></div>);
    }
  }
}

function RenderDish({dish}) {
  if (dish != null) {
    return(
      <FadeTransform
            in
            transformProps={{
                exitTransform: 'scale(0.5) translateY(-50%)'
            }}>
        <Card>
            <CardImg top src={baseUrl + dish.image} alt={dish.name} />
            <CardBody>
                <CardTitle>{dish.name}</CardTitle>
                <CardText>{dish.description}</CardText>
            </CardBody>
        </Card>
      </FadeTransform>
    );     
  } 
  else {
    return(
        <div></div>
    );
  }
}

function RenderComments({myComments, tg}) {
  if (myComments != null) {

    const ListItems = myComments.map((comment) =>
        <li key={comment.id}>
            <p>{comment.comment}</p>
            <p>-- {comment.author}, 
            {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(comment.date)))}</p>
        </li>
       );
    return(
        <div>
          <h4>Comments</h4>
          <Stagger in>
            <Fade in>
              <ul>{ListItems}</ul>
            </Fade>
          </Stagger>
          <Button outline onClick={tg}><span className="fa fa-edit fa-lg"></span> Submit Comment</Button>
        </div>
    );
  }
  else {  
    return(
        <div></div>
    );
  }
}

export default CommentForm;

